FROM docker-proxy.kar-tel.local/node:14.5.0
WORKDIR app
COPY . /app
EXPOSE 8080
CMD ["npm", "serve"]
