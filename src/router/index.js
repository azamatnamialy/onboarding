import Router from 'vue-router'
import Vue from "vue";
import { onboardingRoutes }  from "../views/onboarding-view/onboardingRoutes";

Vue.use(Router);

const router = new Router({
    mode: 'history',
    routes:[
        ...onboardingRoutes,
        {
            path: '*',
            redirect: '/onboarding'
        }
    ]
});

export default router;