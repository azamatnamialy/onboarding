import Vue from 'vue'
import App from './App.vue'
import router from "./router/index";
import { registerBaseComponents } from '@/helpers/registerBaseComponent'

registerBaseComponents(Vue)

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
