export const loadView = path => {
    return () => import(`@/views/${path}.vue`)
}