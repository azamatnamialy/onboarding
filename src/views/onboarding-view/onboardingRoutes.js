export const onboardingRoutes = [
    {
        name: 'Onboarding',
        path: '/onboarding',
        component: () => import('./OnboardingView')
    }
]